/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#ifndef HELPERS_H
#define HELPERS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <regex.h>

/* Strings and literals */
#define MAX_INPUT_BUFFER 50
#define CONSOLE_INPUT "$  "
#define SPACE " "
#define COMMA ","
#define NEWLINE "\n"
#define VERTICAL_BAR "|"
#define BLOCKED_SYMBOL "*"

/* Error and Warning Messages */
#define INVALID_SYNTAX "[ !!! ] Invalid Syntax! Below are list of the available syntaxs.\n"
#define SELECTED_BOARD_OUT_OF_BOUND "[  !  ] Sorry... We only have %d boards!\n"
#define HELP_MESSAGE "[  !  ] Below are available commands:\n"
#define WRONG_GRAMMAR "[ !!! ] Wrong argument!\n"
#define CURRENT_DIRECTION "Current direction: %s\n"
#define RETURN_MAIN_MENU "Returning to main menu...\n\n"
#define ERROR_MESSAGE_CRITICAL "Something happened... An error occured...\n"
#define ILLEGAL_SYNTAX_FOR_CURRENT_STATE "[  ✕  ] Illegal Syntax! Functions that are allowed for now are:\n"
#define UNKNOWN_SYNTAX "[  ✕  ] Unknown Syntax! Refer to instructions below to operate the game:\n"
#define BOARD_OUT_OF_BOUND "[  !  ] The board is only 10x10, and the numbering starts from 0 to 9.\n"
#define BLOCKED_COORDINATE "[  !  ] Unable to place at position [%d,%d]. That location is blocked!\n"
#define PLAYER_MOVES "Player Moves (Steps): %d\n"

/* UI and Help Messages */
#define UI_COMMANDS "Command:\n"
#define UI_BYE "Good bye...\n"
#define HELP_LOAD "\tload <g>\n"
#define HELP_LOAD_EXTENDED "\t\tg: Game board number that is going to be loaded {e.g. 1, 2, ...}\n"
#define HELP_INIT "\tinit <x>,<y>,<direction>\n"
#define HELP_INIT_EXTENDED "\t\tx: Horizontal position of the car on the board (between 0 ... 9)\n\t\ty: Vertical position of the car on the board (between 0 ... 9)\n\t\tdirection: Direction of the car's movement (north, east, south, west)\n"
#define HELP_FORWARD "\tforward [or 'f'] : To move forward\n"
#define HELP_LEFT "\tturn_left [or 'l'] : To turn left\n"
#define HELP_RIGHT "\tturn_right [or 'r'] : To turn right\n"
#define HELP_HELP "\thelp [or 'h'] : Display this help message again\n"
#define HELP_QUIT "\tquit [or 'q'] : Exit the game\n"
#define UI_WELCOME "Welcome to Car Board\n\n"
#define INSTRUCTION "\t--- Instruction ---\n1) Play game\n2) Show student's information\n3) Quit\nPlease enter your choice: "
#define UI_WRONG_CHOICE "Unknown choice! Please select the numbers based on the instruction given!\n\n"
#define UI_INITIAL_HELP "\n\t==== How to *command* ====\nThis game is a simulation game where you can use command to move your own car. Perfect for people who wants to learn how to code.\nIn order to play the game, You will need to learn what syntaxs are available on this game to move your car, or load a map into the game.\n\n"
#define UI_READY "Ready? Press enter to start!"

/* Public typedef enums are declared here for universal access */
typedef enum boolean
{
    FALSE = 0,
    TRUE
} Boolean;

typedef struct alias{
    char command[MAX_INPUT_BUFFER];
    char shortCommand[2];
} Alias;

/* Used for saving the game state, whether it is first timer, or second, or
 after that, which has different command parser */
typedef enum gameState{
    FIRST,
    SECOND,
    PLAY,
    EXIT
} State;

/* Used for function parser */
typedef enum functions
{
    INVALID,
    LOAD,
    INIT,
    FORWARD,
    LEFT,
    RIGHT,
    HELP,
    QUIT,
    NOT_ALLOWED
} Functions;

#define NEW_LINE_SPACE 1
#define NULL_SPACE 1

/**
 * This is used to compensate for the extra character spaces taken up by
 * the '\n' and '\0' when input is read through fgets().
 **/
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)

#define EMPTY_STRING ""


/**
 * Call this function whenever you detect buffer overflow but only call this
 * function when this has happened.
 **/
void readRestOfLine(void);
void enterToContinue(void);

/**
 * This function will return a number that is given by the user from the
 * Console. It also handles some type errors and stream errors using given
 * function from the startup code (helpers.c)
 **/
int scanNumber(void);

#endif
