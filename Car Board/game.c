/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#include "game.h"
#include "carboard.h"

Cell board[BOARD_HEIGHT][BOARD_WIDTH];

void playGame(void)
{
    /*
     * Used to save the state of the program whether it is the first step of
     * the game or not. Actually it is made because it is faster rather than
     * recomparing values.
     */
    Player* player = NULL;
    State gamestate;

    int iterator = 0;
    Boolean exec = FALSE;
    Functions function = 0;
    char cmdInput[MAX_INPUT_BUFFER] = {0};
    char *cache;

    /* Initialise the main component */
    player = malloc(sizeof(Player));
    player->moves = 0;
    gamestate = FIRST;
    initialiseBoard(board);

    displayBoard(board, player);

    /* Game syntax parser will always loop itself until q or quit is executed*/
    while(gamestate != EXIT)
    {
        printf(CONSOLE_INPUT);
        if(fgets(cmdInput, MAX_INPUT_BUFFER, stdin))
        {
            if((cache = strchr(cmdInput, '\n'))){
                *cache = 0;
            } else {
                scanf("%*[^\n]");
                scanf("%*c");
            }
        }
        for(iterator = 0; cmdInput[iterator]; iterator++){
            cmdInput[iterator] = tolower(cmdInput[iterator]);
            if(cmdInput[iterator] == '\n')
                cmdInput[iterator] = '\0';
        }

        cache = strtok(cmdInput, SPACE);
        if(cache == NULL){
            continue;
        }
        function = functionParser(cache);

        /* Check game status after parsing the commands. This is the game flow rule */
        switch(gamestate)
        {
            case FIRST:
                switch(function)
            {
                case LOAD:
                case HELP:
                case QUIT:
                    exec = TRUE;
                    break;
                case INIT:
                case LEFT:
                case RIGHT:
                case FORWARD:
                case NOT_ALLOWED:
                    printf(ILLEGAL_SYNTAX_FOR_CURRENT_STATE);
                    functionInstruction(LOAD);
                    functionInstruction(HELP);
                    functionInstruction(QUIT);
                    exec = FALSE;
                    break;
                case INVALID:
                    printf(UNKNOWN_SYNTAX);
                    printInstruction();
                    exec = FALSE;
                    break;
            }
                break;
            case SECOND:
                if(function == FORWARD || function == LEFT || function == RIGHT){
                    printf(ILLEGAL_SYNTAX_FOR_CURRENT_STATE);
                    printf(UI_COMMANDS);
                    functionInstruction(LOAD);
                    functionInstruction(HELP);
                    functionInstruction(QUIT);
                    functionInstruction(INIT);
                    exec = FALSE;
                }
                break;
            case PLAY:
                exec = TRUE;
                break;
            case EXIT:
                gamestate = EXIT;
                printf(RETURN_MAIN_MENU);
                break;
            default:
                printf(ERROR_MESSAGE_CRITICAL);
                break;
        }
        cache = strtok(NULL, SPACE);

        /* strtok used to tokenise the input so it will be ready to be passed
         to execute() function which only accept parameters */
        if(exec)
            execute(function, cache, player, &gamestate);
        exec = TRUE;
    }
}

Functions functionParser(char toBeParsed[])
{
    Functions method = INVALID;
    int cache[2] = {0,0};
    char* commands[] = {COMMAND_LOAD,COMMAND_INIT,COMMAND_FORWARD,COMMAND_TURN_LEFT,COMMAND_TURN_RIGHT,COMMAND_HELP,COMMAND_QUIT,NULL};
    Alias commandAlias[] = {{COMMAND_TURN_LEFT,{COMMAND_TURN_LEFT_SHORTCUT}}, {COMMAND_TURN_RIGHT,{COMMAND_TURN_RIGHT_SHORTCUT}}, {COMMAND_HELP, {COMMAND_HELP_SHORTCUT}}, {COMMAND_QUIT, {COMMAND_QUIT_SHORTCUT}}, {COMMAND_FORWARD, {COMMAND_FORWARD_SHORTCUT}}};
    for(cache[0] = 0 ; commands[cache[0]] != '\0' ; ++cache[0])
    {
        /* Replace the shorthand command with original command */
        for (cache[1] = 0; cache[1] < (sizeof(commandAlias) / sizeof(Alias)); ++cache[1])
            if(strcmp(commandAlias[cache[1]].shortCommand, toBeParsed) == 0)
                toBeParsed = commandAlias[cache[1]].command;

        if(strcmp(commands[cache[0]], toBeParsed) == 0)
            method = cache[0] + 1;
    }
    return method;
}

void execute(Functions function, char argument[], Player * player, State* gamestate)
{
    Position* position;
    Direction* direction;
    char* param;
    char* cacheString;
    int cache[2] = {0,0};
    int argumentLength = 0;
    if(argument != '\0')
        for(argumentLength = 0; argument[argumentLength] != '\0'; ++argumentLength);
    switch(function)
    {
        case NOT_ALLOWED:
            printf(ILLEGAL_SYNTAX_FOR_CURRENT_STATE);
            return;
        case INVALID:
            printf(INVALID_SYNTAX);
            printInstruction();
            break;
        case LOAD:
            if(argument == NULL){
                wrongGrammarInfo(function);
                return;
            }
            if(!isdigit(argument[0])){
                wrongGrammarInfo(function);
                return;
            }
            cache[0] = (int) strtol(argument, &cacheString, 10);
            if(cache[0] > 2 || cache[0] < 1){
                printf(SELECTED_BOARD_OUT_OF_BOUND, BOARD_COUNT);
                return;
            }
            if(cache[0] == 1)
                loadBoard(board, BOARD_1);
            else
                loadBoard(board, BOARD_2);
            *gamestate = SECOND;
            /* Reset player */
            player = malloc(sizeof(Player));
            player->moves = 0;
            break;
        case INIT:
            if(argument == NULL){
                wrongGrammarInfo(function);
                return;
            }
            param = strtok(argument, COMMA);
            cache[0] = (int) strtol(param, &cacheString, 10);
            param = strtok(NULL, COMMA);
            if(param == NULL){
                wrongGrammarInfo(function);
                return;
            }
            cache[1] = (int) strtol(param, &cacheString, 10);
            param = strtok(NULL, COMMA);
            if(param == NULL){
                wrongGrammarInfo(function);
                return;
            }

            if(!compassCheck(param))
            {
                wrongGrammarInfo(function);
                return;
            }
            position = malloc(sizeof(Position));
            position->x = cache[0];
            position->y = cache[1];
            direction = malloc(sizeof(Direction));
            *direction = parseDirection(param);
            if(placePlayer(board,*position)){
                initialisePlayer(player, position, *direction);
                *gamestate = PLAY;
            }
            break;
        case FORWARD:
            movePlayerForward(board, player);
            break;
        case LEFT:
            turnDirection(player, TURN_LEFT);
            break;
        case RIGHT:
            turnDirection(player, TURN_RIGHT);
            break;
        case HELP:
            printf(HELP_MESSAGE);
            printInstruction();
            return;
        case QUIT:
            *gamestate = EXIT;
            return;
    }
    displayBoard(board, player);
    if(*gamestate == PLAY)
        displayDirection(player->direction);
}

Direction parseDirection(char dir[])
{
    if(strcmp(dir, DIRECTION_NORTH) == 0)
        return NORTH;
    if(strcmp(dir, DIRECTION_SOUTH) == 0)
        return SOUTH;
    if(strcmp(dir, DIRECTION_WEST) == 0)
        return WEST;
    if(strcmp(dir, DIRECTION_EAST) == 0)
        return EAST;
    return NORTH;
}

void wrongGrammarInfo(Functions function)
{
    printf(WRONG_GRAMMAR);
    printf(UI_COMMANDS);
    functionInstruction(function);
    return;
}

Boolean compassCheck(char test[])
{
    int iterator = 0;
    char* dictionary[] = {DIRECTION_EAST, DIRECTION_WEST, DIRECTION_NORTH, DIRECTION_SOUTH};
    for(iterator = 0; iterator < (sizeof(dictionary) / sizeof(char*)) ; ++iterator)
        if(strcmp(test, dictionary[iterator]) == 0)
            return TRUE;
    return FALSE;
}
