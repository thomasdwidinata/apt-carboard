/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#include "helpers.h"

void readRestOfLine()
{
    int ch;
    while((void)(ch = getc(stdin)), ch != EOF && ch != '\n')
    { } /* Gobble each character. */
    
    /* Reset the error status of the stream. */
    clearerr(stdin);
}

void enterToContinue()
{
    char c;
    getchar();
    printf(NEWLINE);
    while ((c = getchar()) != '\n' && c != EOF) { }
}

int scanNumber(void)
{
    int num = 0;
    int test = EOF;
    while(test == EOF)
    {
        test = scanf("%d", &num);
        if(test == EOF){
            readRestOfLine();
        }
        else if(test == 0){
            readRestOfLine();
        }
        else
            return num;
    }
    return num;
}
