/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#include "board.h"

Cell BOARD_1[BOARD_HEIGHT][BOARD_WIDTH] =
{
    { BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, BLOCKED, EMPTY, BLOCKED, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED }
};

Cell BOARD_2[BOARD_HEIGHT][BOARD_WIDTH] =
{
    { BLOCKED, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY }
};

void initialiseBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH])
{
    int row = 0;
    int col = 0;
    for(row = 0; row < BOARD_HEIGHT; ++row)
        for(col = 0; col < BOARD_WIDTH; ++col)
            board[row][col] = EMPTY;
}

void loadBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
               Cell boardToLoad[BOARD_HEIGHT][BOARD_WIDTH])
{
    /* Copy the array contents from boardToLoad[][] to board[][] */
    int row = 0;
    int col = 0;
    for(row = 0; row < BOARD_HEIGHT; ++row)
        for(col = 0; col < BOARD_WIDTH; ++col)
            board[row][col] = boardToLoad[row][col];
}

Boolean placePlayer(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Position position)
{
    int row,col = 0;
    /* Making sure that the input is not outside the board */
    switch(placeable(board, position))
    {
        case PLAYER_MOVED:
            for(row = 0; row < BOARD_HEIGHT; ++row)
                for(col = 0; col < BOARD_WIDTH; ++col)
                    if(board[row][col] == PLAYER)
                        board[row][col] = EMPTY;
            board[position.y][position.x] = PLAYER;
            return TRUE;
        case CELL_BLOCKED:
        case OUTSIDE_BOUNDS:
            return FALSE;
    }
    return FALSE;
}

PlayerMove placeable(Cell board[BOARD_HEIGHT][BOARD_WIDTH], const Position position)
{
    if(position.x > 9 || position.x < 0 || position.y > 9 || position.y < 0){
        printf(BOARD_OUT_OF_BOUND);
        return OUTSIDE_BOUNDS;
    }
    if(board[position.y][position.x] == BLOCKED){
        printf(BLOCKED_COORDINATE, position.x, position.y);
        return CELL_BLOCKED;
    }
    else
        return PLAYER_MOVED;
}

PlayerMove movePlayerForward(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Player * player)
{
    Position next = getNextForwardPosition(player);
    PlayerMove moveStatus = placeable(board, next);
    if(moveStatus == PLAYER_MOVED){
        if(!placePlayer(board, next)){
            moveStatus = CELL_BLOCKED;
        }
        else{
            updatePosition(player, next);
        }
    }
    return moveStatus;
}

void displayBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Player * player)
{
    int col = 0;
    int row = 0;
    printf(VERTICAL_BAR" "VERTICAL_BAR);
    for(col = 0; col < BOARD_WIDTH ; ++col)
        printf("%d"VERTICAL_BAR, col);
    printf(NEWLINE);
    for(row = 0; row < BOARD_HEIGHT; ++row){
        printf(VERTICAL_BAR"%d"VERTICAL_BAR, row);
        for(col = 0; col < BOARD_WIDTH; ++col){
            switch(board[row][col])
            {
                case EMPTY:
                    printf(" "VERTICAL_BAR);
                    break;
                case PLAYER:
                    switch(player->direction)
                {
                    case NORTH:
                        printf(DIRECTION_ARROW_OUTPUT_NORTH);
                        break;
                    case EAST:
                        printf(DIRECTION_ARROW_OUTPUT_EAST);
                        break;
                    case SOUTH:
                        printf(DIRECTION_ARROW_OUTPUT_SOUTH);
                        break;
                    case WEST:
                        printf(DIRECTION_ARROW_OUTPUT_WEST);
                        break;
                }
                    printf(VERTICAL_BAR);
                    break;
                case BLOCKED:
                    printf(BLOCKED_SYMBOL VERTICAL_BAR);
                    break;
            }
        }
        printf(NEWLINE);
    }
    printf(PLAYER_MOVES, player->moves);
}
