/******************************************************************************x
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#include "player.h"
#include "board.h"
#include "carboard.h"

void initialisePlayer(Player* player, Position* position, Direction direction)
{
    /* Directly edit the content of the pointer */
    player->position = *position;
    player->direction = direction;
    player->moves = 0;
    return;
}

void turnDirection(Player * player, TurnDirection turnDirection)
{
    Direction dir;
    switch(turnDirection)
    {
        case TURN_LEFT:
            switch(player->direction)
        {
            case NORTH:
                dir = WEST;
                break;
            case EAST:
                dir = NORTH;
                break;
            case SOUTH:
                dir = EAST;
                break;
            case WEST:
                dir = SOUTH;
                break;
        }
            break;
        case TURN_RIGHT:
            switch(player->direction)
        {
            case NORTH:
                dir = EAST;
                break;
            case EAST:
                dir = SOUTH;
                break;
            case SOUTH:
                dir = WEST;
                break;
            case WEST:
                dir = NORTH;
                break;
        }
            break;
    }
    player->direction = dir;
    return;
}

Position getNextForwardPosition(const Player * player)
{
    Position *newPosition = malloc(sizeof(Position));
    Position pos;
    newPosition->x = player->position.x;
    newPosition->y = player->position.y;
    
    switch(player->direction)
    {
        case NORTH:
            newPosition->y = newPosition->y - 1;
            break;
        case SOUTH:
            newPosition->y = newPosition->y + 1;
            break;
        case WEST:
            newPosition->x = newPosition->x - 1;
            break;
        case EAST:
            newPosition->x = newPosition->x + 1;
            break;
    }
    pos = *newPosition;
    return pos;
}

void updatePosition(Player * player, Position position)
{
    player->position.x = position.x;
    player->position.y = position.y;
    player->moves = player->moves + 1;
    return;
}

void displayDirection(Direction direction)
{
    char* dir;
    switch(direction)
    {
        case NORTH:
            dir = DIRECTION_NORTH;
            break;
        case EAST:
            dir = DIRECTION_EAST;
            break;
        case SOUTH:
            dir = DIRECTION_SOUTH;
            break;
        case WEST:
            dir = DIRECTION_WEST;
            break;
    }
    printf(CURRENT_DIRECTION, dir);
}
