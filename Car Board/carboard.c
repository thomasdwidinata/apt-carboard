/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#include "carboard.h"

int main(int argc, char* args[])
{
    showStudentInformation();
    
    /* Menu (Home)
     * I don't define these strings here because it's only a home screen
     */
    printf(UI_WELCOME);
    while(TRUE)
    {
        printf(INSTRUCTION);
        /* A magic happend here, see helper.h for these definitions {scanNumber()} */
        switch(scanNumber())
        {
            default:
                printf(UI_WRONG_CHOICE);
                break;
            case 1:
                printf(UI_INITIAL_HELP);
                printInstruction();
                printf(UI_READY);
                enterToContinue();
                playGame();
                break;
            case 2:
                showStudentInformation();
                break;
            case 3:
                printf(UI_BYE);
                exit(EXIT_SUCCESS);
                break;
        }
    }
    
    return EXIT_SUCCESS;
}

void printInstruction()
{
    /* A magic happend here, see helper.h for these definitions */
    printf(UI_COMMANDS);
    printf(HELP_LOAD);
    printf(HELP_LOAD_EXTENDED);
    printf(HELP_INIT);
    printf(HELP_INIT_EXTENDED);
    printf(HELP_FORWARD);
    printf(HELP_LEFT);
    printf(HELP_RIGHT);
    printf(HELP_HELP);
    printf(HELP_QUIT);
    return;
}

void functionInstruction(Functions function)
{
    switch(function)
    {
        case INVALID:
        default:
            printInstruction();
            break;
        case LOAD:
            printf(HELP_LOAD);
            printf(HELP_LOAD_EXTENDED);
            break;
        case INIT:
            printf(HELP_INIT);
            printf(HELP_INIT_EXTENDED);
            break;
        case FORWARD:
            printf(HELP_FORWARD);
            break;
        case LEFT:
            printf(HELP_LEFT);
            break;
        case RIGHT:
            printf(HELP_RIGHT);
            break;
        case HELP:
            printf(HELP_HELP);
            break;
        case QUIT:
            printf(HELP_QUIT);
            break;
    }
    return;
}

/*
 * Strings here are left intended because of readability of the
 * ASCII art...
 */
void showStudentInformation()
{
    printf("\n\n----------------------------------------------");
    printf("\n\t\t!--- Car Board ---!\n");
    /* Just an ASCII Art of a Car... */
    printf("\t\t\t  ______\n"
           "\t\t\t /|_||_\\`.__\n"
           "\t\t\t(   _    _ _\\\n"
           "\t\t\t=`-(_)--(_)-'\n\n");
    printf("Made by\t\t: %s\n", STUDENT_NAME);
    printf("Student ID\t: %s\n", STUDENT_ID);
    printf("Student E-mail\t: %s\n", STUDENT_EMAIL);
    printf("----------------------------------------------\n\n");
}
