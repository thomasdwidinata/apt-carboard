/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#ifndef GAME_H
#define GAME_H

#include "player.h"
#include "board.h"
#include "helpers.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define COMMAND_LOAD "load"
#define COMMAND_INIT "init"
#define COMMAND_FORWARD "forward"
#define COMMAND_FORWARD_SHORTCUT "f"
#define COMMAND_TURN_LEFT "turn_left"
#define COMMAND_TURN_LEFT_SHORTCUT "l"
#define COMMAND_TURN_RIGHT "turn_right"
#define COMMAND_TURN_RIGHT_SHORTCUT "r"
#define COMMAND_HELP "help"
#define COMMAND_HELP_SHORTCUT "h"
#define COMMAND_QUIT "quit"
#define COMMAND_QUIT_SHORTCUT "q"

#define DIRECTION_NORTH "north"
#define DIRECTION_EAST "east"
#define DIRECTION_SOUTH "south"
#define DIRECTION_WEST "west"

/**
 * Main menu option 1 - play the game as per the specification.
 *
 * This function makes all the calls to board & player and handles interaction
 * with the user (reading input from the console, error checking, etc...).
 *
 * It should be possible to break this function down into smaller functions -
 * but this is up to you to decide and is entirely your own design. You can
 * place the additional functions in this header file if you want.
 *
 * Note that if you don't break this function up it could become pretty big...
 */
void playGame(void);

/**
 * Syntax Parser
 *
 * The router of the Syntax Parsing to Function Caller. It receives raw input
 * words and then identify the first word what does the user want to execute.
 * Then it will return an Enum Value (typedef enum functions). It also
 * automatically parse the shorthand commands and lowering all cases, so the
 * syntax is not case sensitive.
 *
 */
Functions functionParser(char[]);

void execute(Functions, char[], Player*, State*);

Boolean compassCheck(char[]);

void wrongGrammarInfo(Functions);

Direction parseDirection(char[]);

#endif
