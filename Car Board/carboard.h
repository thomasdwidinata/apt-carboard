/******************************************************************************
 ** Student name: 	Thomas Dwi Dinata
 ** Student number: 	s3702763
 ** Course: 			Advanced Programming Techniques - S1 2018
 ******************************************************************************/

#ifndef CARBOARD_H
#define CARBOARD_H

#include "helpers.h"
#include "game.h"

#define STUDENT_NAME "Thomas Dwi Dinata"
#define STUDENT_ID "s3702763"
#define STUDENT_EMAIL "s3702763@student.rmit.edu.au"

#define DEBUG_MODE FALSE

/**
 * Main menu option 1 - showing the instruction of how to play before playing
 * the game itself. Can be used by Syntax Parser when unknown syntax is given
 */
void printInstruction(void);


/**
 * Main menu option 2 - show your student information as per the specification.
 *
 * You should change the defines above related to student information and use
 * them when printing.
 */
void showStudentInformation(void);


/**
 * Display a specific function command help. Used by Syntax Parser when invalid
 * argument given on a valid function name.
 */
void functionInstruction(Functions function);

#endif
