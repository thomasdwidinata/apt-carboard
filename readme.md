# Advanced Programming Techniques (COSC 1076)

## Car Board / Assignment 1

### Description
The purpose of this assignment is to familiarise students to C Programming Language. The assignment is to make a game named Car Board. The aim of the game is to move the player around the board without hitting any obstacles. The complete requirements and description are available on the Git repo with file name `Assignment One (v1.0).pdf`. All of the source code here are debugged using Xcode on Mac. To find all the source codes, go to `./Car Board/` directory.

### Compiling the Source Code
In order to comply with the assignment specification, we use ANSI C. We need to give parameters before compiling the C source code. In this case, the source code has been tested on RMIT Jupiter, Saturn, and Titan Server running Linux (RedHat), a my own Personal Computer (PC) running Linux (Debian), and a Mac running MacOS 10.13. The GCC Compiler on RMIT Servers are version 4.8.5 (`gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-16)`), and for my PC has gcc version 6.3.0 (`gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516`), and for my Mac is LLVM version 9.1.0 (`Apple LLVM version 9.1.0 (clang-902.0.39.1)`).

The command to compile the source code according to the assignment specification is as below:

`gcc -ansi -pedantic -Wall -o carboard *.c`

The `-ansi` is required to compile using ANSI C standard (C 1989), `-pedantic` is used to warn the user if one of their codes are not safe for POSIX, and `-Wall` is used to warn the user if there is unsafe operation on the code. `-o` is used to specify output name and `*.c` to include all `.c` files (C Source Codes) to be compiled.

To simplify the compiling command, I have made a `makefile` that will automatically do this job for you. Simply execute `make` to compile the source code. If you want to delete the compiled source code (The Binary File), you can use predefined function on the makefile by executing `make clean` to remove the binary file instead of having executing `rm` command.

### How to run
To run the game, you need to compile the source code first using `make` or execute `gcc` command. See Compiling the Source Code section.

After finish compiling, open the binary file by executing the command `./carboard`. The instruction will pop up and follow the instructions.

### Changes

#### Version 1.1
* Removed "Colour Text" as not mentioned in the assignment specification
* Refactored Global Variables as it is not a good practice of coding
* Changed `atoi()` function to `strtol()` for better and safe conversion
* Added buffer overflow protection inside the game

#### Version 1.0 (Release for Demo Week 4)
* Initial release for Demo Week 4 on the Lab
